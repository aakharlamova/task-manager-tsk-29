package ru.kharlamova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.repository.IUserRepository;
import ru.kharlamova.tm.api.service.IPropertyService;
import ru.kharlamova.tm.api.service.IUserService;
import ru.kharlamova.tm.enumerated.Role;
import ru.kharlamova.tm.exception.authorization.LoginExistException;
import ru.kharlamova.tm.exception.empty.*;
import ru.kharlamova.tm.exception.entity.UserNotFoundException;
import ru.kharlamova.tm.model.User;
import ru.kharlamova.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final IUserRepository userRepository;

    @NotNull private final IPropertyService propertyService;

    @NotNull
    public UserService(@NotNull final IUserRepository userRepository, @NotNull final  IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordHashException();
        user.setPasswordHash(passwordHash);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public Optional<User> setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyPasswordException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        final String hash = HashUtil.salt(propertyService, password);
        user.ifPresent(u -> u.setPasswordHash(hash));
        return user;
    }

    @Nullable
    @Override
    public Optional<User> findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public Optional<User> findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Nullable
    @Override
    public Optional<User> updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName, @Nullable final
            String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Optional<User> user = findById(userId);
        user.ifPresent(u -> {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setMiddleName(middleName);
        });
        return user;
    }

    @NotNull
    @Override
    public Optional<User> lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Optional<User> user = userRepository.findByLogin(login);
        user.ifPresent(u -> u.setLocked(true));
        user.orElseThrow(UserNotFoundException::new);
        return user;
    }

    @NotNull
    @Override
    public Optional<User> unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Optional<User> user = userRepository.findByLogin(login);
        user.ifPresent(u -> u.setLocked(false));
        user.orElseThrow(UserNotFoundException::new);
        return user;
    }

}