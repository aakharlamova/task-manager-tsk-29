package ru.kharlamova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.enumerated.Status;

import javax.validation.constraints.Null;
import java.util.Date;

@Getter
@Setter
public abstract class AbstractBusinessEntity extends AbstractEntity {

    @Nullable
    protected String name = "";

    @Nullable
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected String userId;

    @Nullable
    protected Date dateStart;

    @NotNull
    protected Date created = new Date();

    @NotNull
    public String toString() {
        return id + ": " + name + " - " + description;
    }
    
}
