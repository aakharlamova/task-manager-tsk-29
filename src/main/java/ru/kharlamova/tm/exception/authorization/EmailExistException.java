package ru.kharlamova.tm.exception.authorization;

import ru.kharlamova.tm.exception.AbstractException;

public class EmailExistException extends AbstractException {

    public EmailExistException() {
        super("Error! Another user is already using this email. Please try again.");
    }

}
