package ru.kharlamova.tm.api.repository;
import org.jetbrains.annotations.NotNull;
import ru.kharlamova.tm.api.IBusinessRepository;
import ru.kharlamova.tm.model.Task;
import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Optional<Task> bindTaskByProject(@NotNull String userId, @NotNull String taskId, @NotNull String projectId);

    @NotNull
    Optional<Task> unbindTaskFromProject(@NotNull String userId, @NotNull String projectId);

}
