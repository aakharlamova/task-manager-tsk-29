package ru.kharlamova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

    @NotNull
    Integer getBackupInterval();

    @NotNull
    String getValue(@Nullable String javaOpts, @Nullable String environment, @Nullable String defaultValue);

    @NotNull
    String getValueString(@Nullable String javaOpts, @Nullable String environment, @Nullable String defaultValue);

    @NotNull
    Integer getValueInteger(@Nullable String javaOpts, @Nullable String environment, @Nullable Integer defaultValue);

}
