package ru.kharlamova.tm.api;

import ru.kharlamova.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E>{

}
