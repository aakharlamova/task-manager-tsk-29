package ru.kharlamova.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kharlamova.tm.api.repository.ICommandRepository;
import ru.kharlamova.tm.api.repository.IProjectRepository;
import ru.kharlamova.tm.api.repository.ITaskRepository;
import ru.kharlamova.tm.api.repository.IUserRepository;
import ru.kharlamova.tm.api.service.*;
import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.component.Backup;
import ru.kharlamova.tm.enumerated.Role;
import ru.kharlamova.tm.exception.system.UnknownArgumentException;
import ru.kharlamova.tm.exception.system.UnknownCommandException;
import ru.kharlamova.tm.repository.CommandRepository;
import ru.kharlamova.tm.repository.ProjectRepository;
import ru.kharlamova.tm.repository.TaskRepository;
import ru.kharlamova.tm.repository.UserRepository;
import ru.kharlamova.tm.service.*;
import ru.kharlamova.tm.util.SystemUtil;
import ru.kharlamova.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Set;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull  private final IPropertyService propertyService = new PropertyService();

    @NotNull private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull  private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull  private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull  private  final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull  private final ILoggerService loggerService = new LoggerService();

    @NotNull  private final IUserRepository userRepository = new UserRepository();

    @NotNull  private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull  private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull private final Backup backup = new Backup(this, propertyService);

    private void initData() {
            String testId = userService.create("test", "test", "test@test.ru").getId();
            projectService.add(testId, "Project1", "desc");
            taskService.add(testId, "Task11", "desc");
            taskService.add(testId, "Task12", "desc");
            taskService.add(testId, "Task13", "desc");
            String adminId = userService.create("admin", "admin", Role.ADMIN).getId();
            projectService.add(adminId, "Project2", "desc");
            taskService.add(adminId, "Task21", "desc");
            taskService.add(adminId, "Task22", "desc");
            taskService.add(adminId, "Task23", "desc");
    }

    @SneakyThrows
    private void init() {
        @NotNull final Reflections reflections = new Reflections("ru.kharlamova.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.kharlamova.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run(final String... args) throws Exception {
        loggerService.debug("TEST");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initPID();
        initData();
        init();
        backup.load();
        backup.init();
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.err.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseCommand(final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent()) return;
        @Nullable final AbstractCommand command= commandService.getCommandByName(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public void parseArg(String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownArgumentException(arg);
        command.execute();
    }

    public boolean parseArgs(String[] args) throws Exception {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

}
